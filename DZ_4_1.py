def swap(target_list, item_index1, item_index2):
    target_list[item_index1], target_list[item_index2] = target_list[item_index2], target_list[item_index1]
    return target_list


print(swap(['a', 'b', 'c', 'd', 'e'], 1, 3))
