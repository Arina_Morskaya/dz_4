import re
import string


def valid_password(my_str):
    if len(my_str) > 4:
        if re.findall(r'^[0-9a-z-]+$', my_str):
            if len(re.findall(r'[0-9-]', my_str)) != 0 and len(re.findall(r'[0-9-]', my_str)) % 2 == 0 and \
                    len(re.findall(r'[a-z-]', my_str)) % 2 != 0:
                return True
            else:
                return False


print(valid_password('hj7d8k11a'))

