def result(*args):
    b = 0
    for i in args:
        if bin(i)[2:].count('1') % 2 == 0:
            b += i
    return b * args[-2]


print(result(0, 5, 8, 12, 89, 7))

